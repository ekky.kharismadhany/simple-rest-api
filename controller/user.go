package controller

import (
	"bismillah/database"
	"bismillah/model"
	"errors"
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
	"net/http"
)

type UserRepo struct {
	Db *gorm.DB
}

func New() *UserRepo {
	db := database.SetupDatabaseConnection()
	err := db.AutoMigrate(&model.User{})
	if err != nil {
		panic("Cant migrate to database")
	}
	return &UserRepo{Db: db	}
}

func (repository *UserRepo) CreateUser(ctx *gin.Context)  {
	var user model.User
	ctx.BindJSON(&user)
	err := model.CreateUser(repository.Db, &user)
	if err != nil {
		ctx.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		return
	}
	ctx.SecureJSON(http.StatusOK, user)
}

func (repository *UserRepo) GetUsers(ctx *gin.Context) {
	var user []model.User
	err := model.SelectUser(repository.Db, &user)
	if err != nil {
		ctx.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		return
	}
	if errors.Is(err, gorm.ErrRecordNotFound) {
		ctx.AbortWithStatusJSON(http.StatusNotFound, gin.H{
			"error": err.Error(),
		})
		return
	}
	ctx.SecureJSON(http.StatusOK, user)
}

func (repository *UserRepo) GetUser(ctx *gin.Context) {
	var user model.User
	var id, _ = ctx.Params.Get("id")
	err := model.SelectUserById(repository.Db, &user, id)
	if err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			ctx.AbortWithStatus(http.StatusNotFound)
		}
		ctx.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{
			"error": err,
		})
	}
	ctx.SecureJSON(http.StatusOK, user)
}

func (repository *UserRepo) UpdateUser(ctx *gin.Context) {
	var user model.User
	var id, _ = ctx.Params.Get("id")
	err := model.SelectUserById(repository.Db, &user, id)
	if err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			ctx.AbortWithStatusJSON(http.StatusNotFound, gin.H{
				"error": err,
			})
			return
		}
		ctx.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{
			"error": err,
		})
		return
	}
	ctx.BindJSON(&user)
	err = model.UpdateUser(repository.Db, &user)
	if err != nil {
		ctx.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{
			"error": err,
		})
		return
	}
	ctx.SecureJSON(http.StatusOK, user)
}

func (repository *UserRepo) DeleteUser(ctx *gin.Context) {
	var user model.User
	var id, _ = ctx.Params.Get("id")
	err := model.DeleteUser(repository.Db, &user, id)
	if err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			ctx.AbortWithStatusJSON(http.StatusNotFound, gin.H{
				"error": err,
			})
			return
		}
		ctx.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{
			"error": err,
		})
		return
	}
	ctx.SecureJSON(http.StatusOK, gin.H{
		"message": "User deleted successfully",
	})
}
