package main

import (
	"bismillah/controller"
	"github.com/gin-gonic/gin"
)

func main()  {
	router := gin.Default()
	userRepo := controller.New()
	router.GET("/users", userRepo.GetUsers)
	router.GET("/users/:id", userRepo.GetUser)
	router.POST("/users", userRepo.CreateUser)
	router.PUT("/users/:id", userRepo.UpdateUser)
	router.DELETE("users/:id", userRepo.DeleteUser)
	router.Run(":8000")
}
